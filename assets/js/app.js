var _app = function () {
    this.effects = ["bounce", "flash", "pulse", "rubberBand", "shake", "swing", "tada", "wobble", "jello"];
    this.volume = 0.12;
    this.fadeIn = 4000;
    this.audio = null;
    this.id = 0;
    this.shouldIgnoreGif = false;
    this.brandDescription = ["Pentester", "Exploit Dev", "Security Researcher", "CTF Player"];
};

var app = new _app();
